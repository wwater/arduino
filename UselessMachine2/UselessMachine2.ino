// Pin usage with the Motorshield
// ---------------------------------------
// Analog pins: not used at all
//     A0 ... A5 are still available
//     They all can also be used as digital pins.
//     Also I2C (A4=SDA and A5=SCL) can be used.
//     These pins have a breadboard area on the shield.
// Digital pins: used: 3,4,5,6,7,8,9,10,11,12
//     Pin 9 and 10 are only used for the servo motors.
//     Already in use: 0 (RX) and 1 (TX).
//     Unused: 2,13
//     Pin 2 has an soldering hole on the board, 
//           easy to connect a wire.
//     Pin 13 is also connected to the system led.
// I2C is possible, but SPI is not possible since 
// those pins are used.

#include <NewPing.h>
#include <AFMotor.h>
#include <Servo.h>

AF_DCMotor motorLeft(1, MOTOR12_64KHZ); //MOTOR12_64KHZ, MOTOR12_8KHZ, MOTOR12_2KHZ, or MOTOR12_1KHZ
AF_DCMotor motorRight(2, MOTOR12_64KHZ);
#define FINGER_PIN 9 // Digital pin 9: Servo #1 control //Digital pin 10: Servo #2 control

#define CLIFF_DETECT_PIN 14 
#define TRIGGER_PIN  16
#define ECHO_PIN     17
#define MAX_DISTANCE 20 //in cm

#define TOGGLE_PIN 15

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);
Servo myFinger;
int toggleCounter;

void setup(void) 
{ 
  pinMode(TRIGGER_PIN, INPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(CLIFF_DETECT_PIN, INPUT);
  pinMode(TOGGLE_PIN, INPUT);
  
  myFinger.attach(FINGER_PIN);
  myFinger.write(0);
  
  motorLeft.setSpeed(255);  // speed from 0 to 255
  motorRight.setSpeed(255);
  
  Serial.begin(9200);
}


void loop(void) 
{ 
  finger();
  if(toggleCounter > 3)
  {
    //move();
  }
}

boolean turningToggleOff = false;
void finger()
{
  boolean toggleOn = !digitalRead(TOGGLE_PIN);
  
  if(toggleOn)
  {
    if(!turningToggleOff){ toggleCounter++; }
    
    turnToggleOff(); 
  }
  else
  {
     getFinterBack();
  }
}

void turnToggleOff()
{
    turningToggleOff = true;
    myFinger.write(167);
}

void getFinterBack()
{
    turningToggleOff = false;
    myFinger.write(65);
}

void move()
{  
    if(!shouldMove())
    {
       stop();
       return; 
    }
    
    if(cliffDetected())
    {
      avoidCliff();
    } 
    else if (obstacleDetected())
    {
      avoidObstacle();
    }
    else
    {
      forward();
    } 
}

long movingStartedAt;
boolean movingStarted;

boolean shouldMove()
{
  return true; //todo remove
    if(!movingStarted)
    {
       movingStartedAt = millis();
       movingStarted = true;
    }
    
    long movingTime = millis() - movingStartedAt;
    if(movingTime > 5000)
    {
       movingStarted = false;
       toggleCounter = 0;
       return false;
    }
    
    return true;
}

boolean obstacleDetected()
{    
    return false;
    myFinger.write(60);
    int uS = sonar.ping_median(1);
    int distanceCm = uS / US_ROUNDTRIP_CM;
    Serial.println(distanceCm);
    
    return distanceCm > 0 && distanceCm < 10;     
}

boolean cliffDetected()
{
    return digitalRead(CLIFF_DETECT_PIN);
}

boolean beenBackwards = false;

void avoidCliff()
{
  backward();
  delay(300);
  spin();
  delay(300); // should turn > 90 degrees
}

void avoidObstacle()
{
   spin();    
}

void stop(void)
{
  motorLeft.run(RELEASE);  
  motorRight.run(RELEASE);  
}

void spin()
{
  motorLeft.run(FORWARD);
  motorRight.run(BACKWARD);  
}

void backward()
{
  motorLeft.run(BACKWARD); 
  motorRight.run(BACKWARD);  
}

void forward()
{
  motorLeft.run(FORWARD);
  motorRight.run(FORWARD);
}
