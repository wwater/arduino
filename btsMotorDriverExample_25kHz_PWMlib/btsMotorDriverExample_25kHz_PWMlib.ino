/*
 /*
 # This Sample code is for testing the DC Motor Driver 2x15A_lite module.
  
 # Editor : Phoebe
 # Date   : 2012.11.6
 # Ver    : 0.1
 # Product: DC Motor Driver 2x15A_lite
 # SKU    : DRI0018
  
 # Description:     
 # Drive 2 motors with this DC Motor Driver module  
  
 # Hardwares:
 1. Arduino UNO
 2. DC Motor Driver 2x15A_lite  
 3. DC motors x2
  
 #Steps:
 1.Connect the M1_PWM & M2_PWM to UNO digital 5 & 6
 2.Connect the M1_EN & M2_EN to UNO digital 4 & 7
 3.Connect +5V & GND to UNO 5V & GND
  
 # Function for current sense and diagnosis,if you want to use
 please connect the IS pins to Arduino
 Connect LA_IS and RA_IS to UNO digital 2 at the same time
 Connect LB_IS and RB_IS to UNO digital 3 at the same time
 */
 
 #include <PWM.h>
 
int E1 = 9;     //M1 Speed Control
int M1 = 7;     //M1 Direction Control
int M2 = 8;     //M1 Direction Control
int LA_IS = 4;
int RA_IS = 5;
int counter=0;
 
int motorSpeed = 180; 
 
void setup(void) 
{ 
  int i;
  pinMode(E1, OUTPUT);
  pinMode(M1, OUTPUT);
  pinMode(M2, OUTPUT);
  pinMode(LA_IS,INPUT);
  pinMode(RA_IS,INPUT);

  Serial.begin(9200);      //Set Baud Rate
  Serial.println("Run keyboard control");
  digitalWrite(E1,LOW);   
  
  //initialize all timers except for 0, to save time keeping functions
  InitTimersSafe(); 

  //sets the frequency for the specified pin
  bool success = SetPinFrequencySafe(E1, 20000);
  
  // somehow the back works ok, but forward hangs at 250 and below..
  
  if(!success)
  {
    Serial.println("pwm frequention not valid");
  }
} 
  
void loop(void) 
{
  
  static unsigned long timePoint = 0;    // current sense and diagnosis,if you want to use this 
   if(millis() - timePoint > 1000){       //function,please show it & don't forget to connect the IS pins to Arduino                                             
     current_sense();
     timePoint = millis();
   }
    
  if(Serial.available()){
    char val = Serial.read();
    if(val != -1)
    {
      switch(val)
      {
      case 'w'://Move Forward
        advance (motorSpeed,motorSpeed);   //move forward in max speed
        break;
      case 's'://Move Backward
        back_off (motorSpeed,motorSpeed);   //move back in max speed
        break;
      case 'z':
        Serial.println("Hello");
        break;
      case 'x':
        stop();
        break;
      case 'q':
         motorSpeed += 5;
         pwmWrite (E1,motorSpeed);
         Serial.println(motorSpeed);
         break;
       case 'e':
         motorSpeed -= 5;
         pwmWrite (E1,motorSpeed);
         Serial.println(motorSpeed);
         break;
      }
    }
    else stop();  
  }
} 
 
void stop(void)                    //Stop
{
  digitalWrite(E1,0); 
  digitalWrite(M1,LOW);   
  digitalWrite(M2,LOW);   
}   
void advance(char a,char b)          //Move forward
{
  pwmWrite (E1,a);      //PWM Speed Control
  digitalWrite(M1,HIGH);   
  digitalWrite(M2,LOW);  
}  
void back_off (char a,char b)          //Move backward
{
  pwmWrite (E1,a);
  digitalWrite(M1,LOW);   
  digitalWrite(M2,HIGH);
}
void current_sense()                  // current sense and diagnosis
{
  int val1=digitalRead(LA_IS);            
  int val2=digitalRead(RA_IS);
  if(val1==HIGH || val2==HIGH){
    counter++;
    if(counter==3){
      counter=0;
      Serial.println("Warning");
    }  
  } 
}
