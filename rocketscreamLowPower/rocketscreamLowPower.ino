#include "LowPower.h"

const int wakeUpPin = 2;
// low = wake up
// useless machine toggle should provide high in OFF mode.

void setup()
{
    // Configure wake up pin as input.
    // This will consumes few uA of current.
    pinMode(wakeUpPin, INPUT);   
    
    // Allow wake up pin to trigger interrupt.
    attachInterrupt(0, wakeUp, CHANGE);
    
    // numbers 0 (on digital pin 2) and 1 (on digital pin 3). 
    
    //    LOW to trigger the interrupt whenever the pin is low,
    //    CHANGE to trigger the interrupt whenever the pin changes value
    //    RISING to trigger when the pin goes from low to high,
    //    FALLING for when the pin goes from high to low.
    //    The Due board allows also:
    //    HIGH to trigger the interrupt whenever the pin is high. (Arduino DUO only)
}

void loop() 
{
    delay(3000);
    GoToSleep();
}

void GoToSleep()
{
    // Enter power down state with ADC and BOD module disabled.
    // Wake up when wake up pin is low.
    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF); 
    
    // Disable external pin interrupt on wake up pin.
  //  detachInterrupt(0);  Why remove interrupt? This way the arduino will never power on again?
}

void wakeUp()
{
    // Just a handler for the pin interrupt.
}
