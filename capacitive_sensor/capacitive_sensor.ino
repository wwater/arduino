#include <CapacitiveSensor.h>

/*
 * https://www.pjrc.com/teensy/td_libs_CapacitiveSensor.html
 * CapitiveSense Library Demo Sketch
 * Paul Badger 2008
 * Uses a high value resistor e.g. 10M between send pin and receive pin
 * Resistor effects sensitivity, experiment with values, 50K - 50M. Larger resistor values yield larger sensor values.
 * Receive pin is the sensor pin - try different amounts of foil/metal on this pin
 */

CapacitiveSensor   cs_4_2 = CapacitiveSensor(9,10);        // 10M resistor between pins 4 (send > 10K) & 2 (recieve = 1k), pin 2 is sensor pin, add a wire and or foil if desired
boolean deviceTouched = false;
    
void setup()                    
{
  pinMode(7, OUTPUT);
  Serial.begin(9600);
}

void loop()                    
{
    long touchValue =  cs_4_2.capacitiveSensor(30);

    if(touchValue > 10)
    {
        if(!deviceTouched)
        {
          Serial.print("Device touched, with pressure: ");
          Serial.println(touchValue);                  // print sensor output 1
          digitalWrite(7, LOW);
        }
        deviceTouched = true;
    }
    else
    {
      if(deviceTouched)
      {
        Serial.println("Released");
        digitalWrite(7, HIGH);  
      }
       deviceTouched = false; 
      
    }
    delay(10);                             // arbitrary delay to limit data to serial port 
}
