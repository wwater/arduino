// http://playground.arduino.cc/InterfacingWithHardware/Nrf24L01
// http://www.elecfreaks.com/wiki/index.php?title=2.4G_Wireless_nRF24L01p
// viewed from print side, one is in corner
// 1 GND          2 VCC (3.3V, 5 will burn!)
// 3 CE (D8)	  4 CSN (D7)
// 5 SCK (D13)    6 MOSI (D11)
// 7 MISO (D12)   9 IRQ (-)

#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>

int redPin = 6;
int greenPin = 5;
int bluePin = 3;

void setup()
{
  Serial.begin(9600);
  Mirf.spi = &MirfHardwareSpi;
  Mirf.init();
  Mirf.setRADDR((byte *)"serv1");
  Mirf.payload = 1;
  Mirf.config();
  setColor(0, 255, 0);  // green

  Serial.println("Listening..."); 
}

void loop()
{
    byte data[Mirf.payload];
    
  if(!Mirf.isSending() && Mirf.dataReady()){
    Serial.print("Got packet ");
    Mirf.getData((byte *)data);

	if (sizeof(data) > 0)
	{
		setColor(data[0]);
	}
  }
}

void setColor(int data)
{
	switch (data)
	{
		case 0:
			setColor(0, 255, 0);  // green
			break;
		case 183:
			setColor(0, 0, 255);  // blue
			break;
		case 219:
			setColor(255, 255, 0);
		default:
			setColor(255, 0, 0);  // red
			break;
	}
}

  

void setColor(int red, int green, int blue)
{
  red = 255 - red;
  green = 255 - green;
  blue = 255 - blue;
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);  
}

