// http://playground.arduino.cc/InterfacingWithHardware/Nrf24L01
// http://www.elecfreaks.com/wiki/index.php?title=2.4G_Wireless_nRF24L01p
// viewed from print side, one is in corner
// 1 GND          2 VCC (3.3V, 5 will burn!)
// 3 CE (D8)	  4 CSN (D9)
// 5 SCK (D13)    6 MOSI (D11)
// 7 MISO (D12)   9 IRQ (-)

// this is the SENDER code
#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>

void setup(){
  Serial.begin(9600);
  Mirf.csnPin = 9;
  Mirf.cePin = 8;
  Mirf.spi = &MirfHardwareSpi;
  Mirf.init();
  Mirf.setRADDR((byte *)"s1");
  Mirf.payload = 1;
  Mirf.config();
  
  Serial.println("Beginning ... "); 
}

void loop(){
  Mirf.setTADDR((byte *)"m1");
  
  Mirf.send((byte *)1); // received as 0
  while (Mirf.isSending()){}
  delay(1000);
  Mirf.send((byte *)2); // recieved as 183
  while (Mirf.isSending()){}
  delay(2000);
  Mirf.send((byte *)3); // recieved as 219
  while (Mirf.isSending()){}
  delay(1000);
  //while(Mirf.isSending()){
  //}
  //  Serial.println("Finished sending");
  //  delay(10);
  //  while(!Mirf.dataReady()){
  //    //Serial.println("Waiting");
  //    if ( ( millis() - time ) > 1000 ) {
  //      Serial.println("Timeout on response from server!");
  //      return;
  //    }
  //}
  //
  //Mirf.getData((byte *) &time);
  //
  //Serial.print("Ping: ");
  //Serial.println((millis() - time));
  //
  
} 
  
  
  

