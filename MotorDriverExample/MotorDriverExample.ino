byte ENA=5;
byte ENB=6;                   
byte IN1=8;                   
byte IN2=9;
byte IN3=10;                   
byte IN4=11;

void setup()
{
  Serial.begin(9600);
 
  pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(ENB, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
}

void loop() {
  MotorUp();
  delay(5000);
  MotorStop();
  delay(5000);
  MotorDown();
  delay(5000);
}

void MotorUp()
{
   digitalWrite(IN1, HIGH);// Set direction
   digitalWrite(IN2, LOW);    
   analogWrite(ENA, 255); // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 100. Default: 70
   digitalWrite(IN3, LOW);// Set direction
   digitalWrite(IN4, HIGH);    
   analogWrite(ENB, 255); // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 100. Default: 70
   //digitalWrite(ENA, LOW);
}

void MotorDown()
{ 
 
   digitalWrite(IN1, LOW); // Set direction
   digitalWrite(IN2, HIGH);    
   analogWrite(ENA, 255); // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 100. Default: 70
   digitalWrite(IN3, HIGH); // Set direction
   digitalWrite(IN4, LOW);    
   analogWrite(ENB, 255); // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 100. Default: 70

}

void MotorStop()
{
   digitalWrite(ENA, HIGH);
   digitalWrite(IN1, HIGH);          
   digitalWrite(IN2, HIGH);
   digitalWrite(IN3, HIGH);          
   digitalWrite(IN4, HIGH);
}

