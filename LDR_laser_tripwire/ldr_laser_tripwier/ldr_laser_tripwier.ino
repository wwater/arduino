// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
boolean isHumanDetected = true;
#define RELAY1  13

void setup() 
{
  Serial.begin(9600);
  pinMode(RELAY1, OUTPUT);   
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Beam laser on ldr to start ...");
}

void loop()
{
  if(analogRead(0) < 750)
  {
    HumanDetected(true);
  } 
  else{
     HumanDetected(false);
  }
    Serial.println(analogRead(0));
}

void HumanDetected(boolean isDetected)
{
  if(isDetected != isHumanDetected)
  {
    isHumanDetected = isDetected;
    if(isHumanDetected)
    {
      lcd.clear();
      lcd.println("Human detected");
      digitalWrite(RELAY1,LOW);  
    }
    else
    {
       lcd.clear();
       lcd.println("All ok"); 
       digitalWrite(RELAY1,HIGH);  
    }
  } 
}
