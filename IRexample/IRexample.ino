//http://garagelab.com/profiles/blogs/tutorial-arduino-ir-sender-and-receiver
// http://www.righto.com/2010/03/detecting-ir-beam-break-with-arduino-ir.html

#include <IRremote.h>

#define PIN_IR 3
#define PIN_DETECT 2
#define PIN_STATUS 13

IRsend irsend;
void setup()
{
  pinMode(PIN_DETECT, INPUT);
  pinMode(PIN_STATUS, OUTPUT);
}

void loop() {
  digitalWrite(PIN_STATUS, !digitalRead(PIN_DETECT));
  irsend.sendSony(0xa90, 12); // Sony TV power code
  delay(10);
}
