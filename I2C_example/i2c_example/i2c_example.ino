#include <Wire.h>

#define LED_PIN 8
#define BUTTON 2

boolean humanDetected;

void setup() {
  Wire.begin(1);                // Start I2C Bus as a Slave (Device Number 9)
  Wire.onReceive(receiveEvent); // register event
  Wire.onRequest(returnAllClear);
  Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);
  pinMode(BUTTON, INPUT);
  digitalWrite(LED_PIN, LOW);

  humanDetected = false;
}

void loop() {
  //digitalWrite(LED_PIN, humanDetected);
}

void receiveEvent(int howMany) {
  humanDetected = Wire.read() == 1;
  digitalWrite(LED_PIN, !humanDetected);
  Serial.println(humanDetected);
}

void returnAllClear()
{  
  Wire.write(!digitalRead(BUTTON));
}

