#include <Wire.h>

int pirPin = 12;  
int ledPin = 11;
boolean humanDetected = false;
boolean blinked = false;
void setup()
{
  Serial.begin(9600);
  pinMode(pirPin, INPUT);
  pinMode(13, OUTPUT);
  pinMode(ledPin, OUTPUT); 
  Wire.begin(); // Start I2C Bus as Master
}
void loop()
{
  boolean humanDetected = digitalRead(pirPin) == HIGH;
  digitalWrite(13, humanDetected);
  Wire.requestFrom(1, 1);
  boolean allClear = false;
  while(Wire.available())    // slave may send less than requested
  { 
    allClear = Wire.read() == 1;    
    if(allClear)
    {
        blinked = !blinked;
    }

    Wire.beginTransmission(1);
    if(humanDetected && allClear)
    {
        Wire.write(blinked); 
        digitalWrite(ledPin, !blinked);
    }
    else
    {
      Wire.write(false); 
      digitalWrite(ledPin, true);
    }
    Wire.endTransmission();    
  }
  Serial.print("Human detected: ");
  Serial.print(humanDetected);
  Serial.print(" Button pressed: ");
  Serial.println(allClear);
  
  delay(100);
}
