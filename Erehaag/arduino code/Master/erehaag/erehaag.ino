// Erehaag MASTER

char* wirelessId = "m1";
char* commandsSlaveWirelessId = "s1"; 

#include "Timer.h"
#include <PWM.h>
#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>

bool debug = true;

byte UPSENSOR = 2;
byte DOWNSENSOR = 3;
byte STOPSENSOR_ERROR_LED = 5;

byte IR_DETECT = 4;
byte PIR = 14;

byte MOTOR_SPEED = 9;
byte MOTOR_CHECK_1 = 16;
byte MOTOR_CHECK_2 = 17;
byte MOTOR_DIRECTION_UP = 18;
byte MOTOR_DIRECTION_DOWN = 19;
byte MOTOR_ERROR_LED = 6;

Timer timer; 

int motorSpeedMin = 0;
int motorSpeedMaxUp = 240;
int motorSpeedMaxDown = 160;
int motorSpeedStep = 5;
int motorSpeedUpdateFrequency = 10; // delay in ms

long maxMotorActionTime = 5000;

long maxTimeBetweenIrSignals = 100;
//if (debug){maxTimeBetweenIrSignals = 10000;} // enable when testing without master slave setup.

int slaveCommand = -66; // Somehow the motoraction can't call CommandSlave directly. So a variable is used as work around. 


void setup()
{
	Serial.begin(9600);
	pinMode(IR_DETECT, INPUT);

	pinMode(PIR, INPUT);
	pinMode(UPSENSOR, INPUT);
	pinMode(DOWNSENSOR, INPUT);
        pinMode(STOPSENSOR_ERROR_LED, OUTPUT);
        
	pinMode(MOTOR_SPEED, OUTPUT);
	pinMode(MOTOR_DIRECTION_UP, OUTPUT);
	pinMode(MOTOR_DIRECTION_DOWN, OUTPUT);
        pinMode(MOTOR_ERROR_LED, OUTPUT);

	digitalWrite(MOTOR_SPEED, LOW);

        InitMotorDriver();
        InitWireless();
}

void InitWireless()
{
  	Mirf.csnPin = 8;
	Mirf.cePin = 7;
	Mirf.spi = &MirfHardwareSpi;
	Mirf.init();
	delay(1000); //startup wireless
	Mirf.setRADDR((byte *) &commandsSlaveWirelessId); //receiving address
	Mirf.payload = sizeof(int);
	Mirf.config();  
}

void InitMotorDriver()
{
  timer.every(motorSpeedUpdateFrequency, updateMotorSpeed);
        
    //initialize all timers except for 0, to save time keeping functions
  InitTimersSafe(); 

  //sets the frequency for the specified pin
  bool success = SetPinFrequencySafe(MOTOR_SPEED, 20000);
  if(!success)
  {
    Serial.println("pwm frequention not valid");
  }
}

void loop()
{
	MotorLogic();
	CommandSlave(slaveCommand);
        timer.update();
}


int previousCommand;
// Somehow this function must be directly below the loop()
void CommandSlave(int action)
{
	if (previousCommand != action)
	{
                DebugLn("CommandSlave: " + String(action));
		previousCommand = action;
		Mirf.setTADDR((byte *) &wirelessId); // 5 bytes long, sending address
                Mirf.send((byte *) &action);
		while (Mirf.isSending()){}
	}
}

void MotorLogic()
{
	if (!IsArmMovementAllowed())
	{
		DebugLn("Beweging niet toegestaan");
		MotorStop();
		slaveCommand = 3;
		return;
	}

	DebugLn("Arm at position: " + String(GetArmPosition()) + "Desired position: " + String(GetDesiredPosition()));

	boolean isAtPosition = GetDesiredPosition() == GetArmPosition();

	slaveCommand = GetDesiredPosition();

	if (isAtPosition || GetArmPosition() == -1)
	{
		MotorStop();
		return;
	}

	ConvertPositionToMotorAction(GetDesiredPosition());
}

int GetDesiredPosition()
{
	if (HumanApproaching())
	{
		return 1;
	}
	return 2;
}

int GetArmPosition()
{
	boolean isUp = !digitalRead(UPSENSOR); // normally closed sensor, so low when activated.
	boolean isDown = !digitalRead(DOWNSENSOR);

	boolean endSensorsNotConnected = isUp && isDown;
	if (endSensorsNotConnected)
	{
                DebugLn("Beide eindstoppen geven geen signaal, kaput??");
		
                digitalWrite(STOPSENSOR_ERROR_LED, HIGH);
                delay(300);
                digitalWrite(STOPSENSOR_ERROR_LED, LOW);
                delay(300);
                
                return -1;
	}
	else
	{
		if (isUp)
		{
			return 1;
		}
		if (isDown)
		{
			return 2;
		}
	}
	return 0;
}

long lastMotionDetectedAt;
long timeAfterLastMotionBeforeArmGoesDown = 5000;
boolean HumanApproaching()
{
	boolean motionDetected = digitalRead(PIR);
	if (motionDetected)
	{
		lastMotionDetectedAt = millis();
	}
	return (millis() - lastMotionDetectedAt) <= timeAfterLastMotionBeforeArmGoesDown;
}

boolean IsArmMovementAllowed()
{
	return !IsObstacleDetected();
}

unsigned long lastIrReceivedAt = millis();
boolean IsObstacleDetected()
{
	if (!digitalRead(IR_DETECT))
	{
		lastIrReceivedAt = millis();
	}

	return (millis() - lastIrReceivedAt) > maxTimeBetweenIrSignals;
}

void ConvertPositionToMotorAction(int position)
{
        CheckMotorStatus();
	switch (position)
	{
	case 1:
		MotorUp();
		break;
	case 2:
		MotorDown();
		break;
	default:
		MotorStop();
		break;
	}
}

int motorDirection = 0; //0=stop, 1=up, 2=down
int motorDirectionPrevious = 0;
int motorSpeedCurrent = 0;
boolean switchingMotorDirection = false;

void updateMotorSpeed()
{
    if(motorDirection == 0)
    {
        motorSpeedCurrent = 0;
        return;
    }
    
    if(motorSpeedCurrent == 0)
    {
      switchingMotorDirection = false;
      motorDirectionPrevious = motorDirection;
    }
    
    if(switchingMotorDirection)
    {
        stopRamped();
        return;
    }
    
    if(motorDirection != motorDirectionPrevious)
    {
        switchingMotorDirection = true;
        return;
    }  
    
    if(motorDirection == 1)
    {
      digitalWrite(MOTOR_DIRECTION_UP,HIGH);   
      digitalWrite(MOTOR_DIRECTION_DOWN,LOW); 
    }
    else if(motorDirection == 2)
    {
      digitalWrite(MOTOR_DIRECTION_UP,LOW);   
      digitalWrite(MOTOR_DIRECTION_DOWN,HIGH);
    }
    startRamped();

}

void startRamped()
{
      int motorSpeedMax = 0;
      if(motorDirection == 1)
      {
          motorSpeedMax = motorSpeedMaxUp;
      }
      if(motorDirection == 2)
      {
          motorSpeedMax = motorSpeedMaxDown;
      }
  
      if(motorSpeedCurrent < motorSpeedMin)
      {
          motorSpeedCurrent = motorSpeedMin;
      }
      
     // van min naar max in stapjes
     int newMotorSpeed = motorSpeedCurrent + motorSpeedStep;
     if(newMotorSpeed <= motorSpeedMax)
     { 
        pwmWrite (MOTOR_SPEED,newMotorSpeed);
        motorSpeedCurrent = newMotorSpeed;
     } 
}

void stopRamped()
{
       // van huidig naar 0 in stapjes
       int newMotorSpeed = motorSpeedCurrent - motorSpeedStep;
       if(newMotorSpeed < motorSpeedMin)
       { 
          newMotorSpeed = 0;
       }
       
       pwmWrite (MOTOR_SPEED,newMotorSpeed);
       motorSpeedCurrent = newMotorSpeed;
}


long lastMotorActionStartedAt = 0;
void MotorUp()
{
        lastMotorActionStartedAt = millis();
        motorDirection = 1;
}

void MotorDown()
{
        lastMotorActionStartedAt = millis();
        motorDirection = 2;
}

void MotorStop()
{
        lastMotorActionStartedAt = 0;
        motorDirection = 0;
	digitalWrite(MOTOR_DIRECTION_UP, LOW);
	digitalWrite(MOTOR_DIRECTION_DOWN, LOW);
	digitalWrite(MOTOR_SPEED, LOW);
}

int motorWarningCounter = 0;
unsigned long lastMotorStatusCheck = 0;
boolean IsMotorStatusOk()
{
   static unsigned long timePoint = 0;
   if(millis() - lastMotorStatusCheck > 1000){
      lastMotorStatusCheck = millis();
      int val1=digitalRead(MOTOR_CHECK_1);            
      int val2=digitalRead(MOTOR_CHECK_2);
      if(val1==HIGH || val2==HIGH){
        motorWarningCounter++;
        Debug("Motor warning");
      }
      else if(motorWarningCounter > 0)
      {
        motorWarningCounter--;
      }
      
   }
  return motorWarningCounter < 3;
}

void CheckMotorStatus()
{
    while(!IsMotorStatusOk()) // wait till motor is ok again
    {
        Debug("Motor not ok");
        MotorStop();
        digitalWrite(MOTOR_ERROR_LED, HIGH);
        delay(300);
        digitalWrite(MOTOR_ERROR_LED, LOW);
        delay(300);
    }
    
    if(lastMotorActionStartedAt != 0)
    {
      long motorActionTime = millis() - lastMotorActionStartedAt;
      while(motorActionTime > maxMotorActionTime) //The motor is running for more than 5 sec, so something is wrong. End stop broken, wrong connection. Reset arduino to exit loop.
      {
        digitalWrite(MOTOR_ERROR_LED, HIGH);
        delay(2000);
        digitalWrite(MOTOR_ERROR_LED, LOW);
        delay(2000);
      }
    }
}

String previousMessage;

void Debug(String message)
{	
	if (debug && message != previousMessage)
	{
		Serial.print(message);
		previousMessage = message;
	}
}

void DebugLn(String message)
{
	if (debug && message != previousMessage)
	{
		Serial.println(message);
		previousMessage = message;
	}
}

void Debug(String message1, String message2)
{
	Debug(message1 + "" + message2);
}
void DebugLn(String message1, String message2)
{
	DebugLn(message1 + "" + message2);
}
