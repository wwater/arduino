/*
Test code for printed Erehaag for demonstration purposes only
Written by: Menno-Jan Rietema
Date: 09-06-2014
Revision: 10
Summary: Two arms with sword move up and down when object is detected in front of one of both ultrasonic sensors



Features:
- uses interrupts 0 and 1 for end switch detection (See for more info on interrupts: http://arduino.cc/en/Reference/AttachInterrupt )
- uses l298n dual H-bridge driver for dc motor control (See for example: http://www.ebay.com/itm/290899895209 or google pictures on "L298n module Arduino")
- uses PWM signal to control speed of 6V motor (Take care not to PWM with too high duty cycle! Motor may overheat when powered by 12V dc supply!)
- uses AN signal from 2 MaxBotix Ultrasonic Rangefinders (LV-MaxSonar-EZ 1 [brown dot]) to react on closing objects. sensors are separately triggered by interrupt function, so they don't disturb each other
- See for info on sensors: - General info and datasheet: http://www.maxbotix.com/Ultrasonic_Sensors/MB1010.htm
                           - for chaining of sensors: http://www.maxbotix.com/articles/031.htm


Circuit details: 
- two end stops for motor A installed on pin 2 and 3. Each pulled LOW by 10K resistor. Resistors are parallel connected with ceramic 22nF capacitor to filter EMC noise from dc motor
- The used dc motor produces lots of electrical noise. Three 10nF ceramic capacitors are connected to filter this. Two from each motor terminal to motor casing, and one between motor terminals. (See for more info: http://www.beam-wiki.org/wiki/Reducing_Motor_Noise )
- Motor shield is powered by V_in on Arduino. Logic power (5V) for shield is onboard created by own voltage regulator with jumper installed on shield (default installed). 
- When power saving is important (eg when running on 9V battery), remove jumper from motor shield and place on circuit board pins. Logic power of motor shield is then supplied by Arduino regulator.
- Green led is connected with 220 Ohm series Resistor to A0
- Red led is connected with 100 Ohm series Resistor A1
- Orange led is connected with 220 Ohm series Resistor to digital pin 5
- two endstops for motor B installed on A2 and A3, see endstops motor A
- furher connections: see PIN declarations
*/


// Variables //////////////////////
//PIN declarations:
byte ENA=6;                   // pin connected to Enable A on motor driver card (to turn motor on or off (or PWM))
byte IN1=7;                   // pin connected to In 1 on motor driver card (to specify direction)
byte IN2=8;                   // pin connected to In 2 on motor driver card (to specify direction)

byte endSwitchLowPinA=2;      // lower end stop switch motor A, polled by interrupt function at 100 Hz
byte endSwitchHighPinA=3;     // higher end stop motor A, polled at 100Hz
byte sonarANPinA = A5;        // connected to MaxSonar AN pin
byte sonarRXPinA = 9;         // connected to MaxSonar RX pin to trigger ranging
byte LedDistance = 5;         // used to give a representation of distance with brightness
byte LedUp = A0;              // on when arm is going up
byte LedDown = A1;            // on when arm is going down
// for second prototype installed:
byte ENB = 11;                // see ENA
byte IN3 =12;
byte IN4 = 13;
byte endSwitchLowPinB=A2;     // lower end stop switch motor B, polled by interrupt function at 100 Hz
byte endSwitchHighPinB=A3;    // higher end stop switch motor B, polled by interrupt function at 100 Hz
byte sonarANPinB = A4;        // AN pin of sensor 2
byte sonarRXPinB = 10;        // RX pin of sensor 2


// NON changable variables:
volatile boolean actionA = false;                   // determines if actionA is to perform
volatile boolean actionB = false;                   // determines if actionB is to perform
volatile boolean nextDirectionUpA = true;           // determines in which direction next actionA will be. True == upwards.
volatile boolean nextDirectionUpB = true;           // determines in which direction next actionB will be. True == upwards.

unsigned long start_up_time = 0;                   // stores time at which last upward movement is started
double avFactor = 0.2;                             // averaging constant, see function readDistance()
double tempDistanceA;                              // contains the distance that is instantly read from sensor A (without filtering/averaging. very fast update. Used to determine when arm has to rise)
double avDistanceA;                                // contains a moving average from the last distance readings of sensor A (lags behind instant reading, but filters inaccurate and unreliable peaks in readings. Used to determine when arm can safely descend )
double tempDistanceB;                              // see above, but for sensor B
double avDistanceB; 
int sensorCount=0;                                 // counter to trigger RX pins in interrupt routine
double brightness=0;                               // temporary value to pwm to orange led on pin 5

//for interrupt states
boolean prevStateHighPinA;                         // used to store previous state of high end switch on motor A, so a state change can be noticed
boolean prevStateLowPinA;
boolean prevStateHighPinB;
boolean prevStateLowPinB;

//USER DEFINED VARIABLES (CAN BE CHANGED TO PREFERENCES)
double reactionDistance = 20;                      // distance of objects that trigger arm rising
int waitTime = 5000;                               // time to wait before arm will descend
boolean prototype2;                                // becomes true after setup if 2 sets of prototype endstops are detected



void setup(){
  
  Serial.begin(115200);                            // initiate serial communication with given baudrate

  // configure pin modes:
  pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(endSwitchHighPinA, INPUT);                // (LOW => switch is pressed)
  pinMode(endSwitchLowPinA, INPUT);                 // (LOW => switch is pressed) 
  pinMode(sonarANPinA, INPUT); 
  pinMode(sonarRXPinA, OUTPUT);
  pinMode(LedDistance, OUTPUT);
  pinMode(LedUp, OUTPUT);
  pinMode(LedDown, OUTPUT);
  // used for prototype 2
  byte ENA2 = 11;
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);    // goed initialiseren, knippert by startup
  pinMode(endSwitchLowPinB, INPUT);
  pinMode(endSwitchHighPinB, INPUT);
  pinMode(sonarANPinB, INPUT);
  pinMode(sonarRXPinB, OUTPUT); 
  
  
  // initial setting of variables:
  nextDirectionUpA = !digitalRead(endSwitchHighPinA); // reads state of high endswitch to determine direction at startup. 
  nextDirectionUpB = !digitalRead(endSwitchHighPinB); // reads state of high endswitch to determine direction at startup. 
  tempDistanceA = reactionDistance;                   // safe starting value for averaged distance readings
  tempDistanceB = reactionDistance;
  prevStateHighPinA = digitalRead(endSwitchHighPinA); // not sure if required
  prevStateLowPinA = digitalRead(endSwitchLowPinA);
  
  if (digitalRead(endSwitchLowPinA)== LOW && digitalRead(endSwitchHighPinA)== LOW){  // endstops are not (properly) connected, may lead to mechanical failure of prototype
  endStopError();
  }
  if (digitalRead(endSwitchLowPinB)== LOW && digitalRead(endSwitchHighPinB)== LOW){  // endstops are not (properly) connected, OR prototype 2 is not installed
  prototype2 = false;
  stopMotorB();
  }
  else{ prototype2 = true;}
  
  // Setting of interrupt function at 100Hz. For more info see: http://www.instructables.com/id/Arduino-Timer-Interrupts/?ALLSTEPS
  cli();//stop interrupts   
  
  //set timer1 interrupt at 1Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 100hz increments
  OCR1A = 156;// = (16*10^6) / (100*1024) - 1 (must be <65536)
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS10 and CS12 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK1 |= (1 << OCIE1A);
  
  sei();//allow interrupts
  
  
     Serial.println("Setup succesful");
}  // end of setup



ISR(TIMER1_COMPA_vect){    //timer1 interrupt @ 100 Hz
//////////////// motor A /////////////////////////
  boolean currStateLowPinA = digitalRead(endSwitchLowPinA);      // reads state of endstops 100 times /s
  boolean currStateHighPinA = digitalRead(endSwitchHighPinA);
  
  if(currStateLowPinA != prevStateLowPinA && !currStateLowPinA && !nextDirectionUpA){ // true when state has changed, state is LOW(pressed), and direction was downwards
    stopMotorA();                            // calls function to stop motor
    actionA=false;                           // tells the loop function that action is not to be performed anymore
    nextDirectionUpA=true;                   // changes direction of next movement
  };
  
  if( currStateHighPinA != prevStateHighPinA && !currStateHighPinA && nextDirectionUpA){
    stopMotorA();                            // calls function to stop motor
    actionA=false;                           // tells the loop function that action is not to be performed anymore
    nextDirectionUpA=false;                  // changes direction of next movement
  };
  prevStateLowPinA = currStateLowPinA;       // store values so they are previous values next time
  prevStateHighPinA = currStateHighPinA;
  
  //////////////// motor B ///////////////////////
  if(prototype2){
  boolean currStateLowPinB = digitalRead(endSwitchLowPinB);
  boolean currStateHighPinB = digitalRead(endSwitchHighPinB);
  
  if(currStateLowPinB != prevStateLowPinB && !currStateLowPinB && !nextDirectionUpB){
    stopMotorB();              // calls function to stop motor
    actionB=false;             // tells the loop function that action is not to be performed anymore
    nextDirectionUpB=true;     // changes direction of next movement
  };
  
  if( currStateHighPinB != prevStateHighPinB && !currStateHighPinB && nextDirectionUpB){
    stopMotorB();              // calls function to stop motor
    actionB=false;             // tells the loop function that action is not to be performed anymore
    nextDirectionUpB=false;    // changes direction of next movement
  };
  prevStateLowPinB = currStateLowPinB;
  prevStateHighPinB = currStateHighPinB;
  }
  ///////////////////////////////////////////////
  
  // Triggering RX pins of sonar devices for distance readings: 
  if (sensorCount == 0){digitalWrite(sonarRXPinB, LOW);    // makes RX pin of sensor B LOW(=off) and RX pin sensor A HIGH (ranging) for 30 ms
                       digitalWrite(sonarRXPinA, HIGH);
                     }
  
  if (sensorCount == 3){digitalWrite(sonarRXPinA, LOW);    // turns all sensors off for 20 ms (otherwise they disturb each others ranging)
                       digitalWrite(sonarRXPinB, LOW);
                      }

  if (sensorCount == 5){digitalWrite(sonarRXPinA, LOW);    // turns B on and A off for 30 ms
                       digitalWrite(sonarRXPinB, HIGH);
                      }
  if (sensorCount == 8){digitalWrite(sonarRXPinA, LOW);    // turns both off for 20 ms
                       digitalWrite(sonarRXPinB, LOW);
                      }
  
  sensorCount +=1;
  if(sensorCount > 9){sensorCount=0;} //reset sensorcount after 100 ms
}

void loop(){

    readDistanceA();        // calls function readDistance, which updates variables tempDistanceA and avDistanceA
    if(prototype2){
     readDistanceB();
     brightness = min(avDistanceA,avDistanceB);    // takes the minimum value to display on orange led
    }
    else{
     brightness = avDistanceA;
    }
    
    brightness = constrain(brightness, 14, 60);    // limits range of sensor values to between 14 and 60 

    brightness = map(brightness, 14, 60, 70, 0);   // maps brightness to see difference better
    analogWrite(LedDistance,brightness);           // writes brightness to led
    
   // for serial reading of distance: decomment code:
    /*
       Serial.print(millis()/1000);                // prints seconds
       Serial.print("\t");                         // prints tab
       Serial.print(avDistanceA);                  // avaraged distance measured by sensor A
       Serial.print("\t");
       Serial.println(avDistanceB);
    */
 
  // Conditions for going upwards motor A:  
  if(!prototype2){                                   // true when only one prototype (A) is installed
        if(tempDistanceA < reactionDistance){        // true if non-filtered distance reading is smaller than given range
           start_up_time = millis();                 // sets start time of going up (so after 'waitTime' arm can go down
           actionA = true;                           // boolean to perform action is set to true for motor A
           nextDirectionUpA=true;                    // sets next direction, so when objects close in during downward movement, movement is reversed
           
        }
   }
  else{
           if(min(tempDistanceA,tempDistanceB) < reactionDistance){    // true if smallest of non-filtered distance readings of A and B is smaller than given range
           start_up_time = millis();                                   // sets start time of going up (so after 'waitTime' arm can go down
           actionA = true;                                             // boolean to perform action is set to true for motor A
           nextDirectionUpA=true;
           actionB=true;
           nextDirectionUpB=true;
        }
  }
  // Conditions for going downwards motor A:
  if(avDistanceA > reactionDistance && millis()-start_up_time > waitTime && digitalRead(endSwitchLowPinA)==HIGH ){    // when filtered distance is large enough AND 'waitTime has passed since going upwards AND the high endstop is not pressed
     actionA = true;                       // boolean to perform actionA is set to true
     nextDirectionUpA = false; 
     }
  
 
 // What to do when an actionA is to be performed:
 if (actionA ){
  if(nextDirectionUpA && digitalRead(endSwitchHighPinA)==HIGH){  // true when direction of arm should be upwards and higher endstop is not pressed
    digitalWrite(IN1, HIGH);                                     // Set direction
    digitalWrite(IN2, LOW);                                      // Set direction
    analogWrite(ENA, 90);                                        // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 120. Default: 90
    digitalWrite(LedUp, HIGH);                                   // turns red LED on
    digitalWrite(LedDown, LOW);                                  // turns green LED off
  }
  if(!nextDirectionUpA && digitalRead(endSwitchLowPinA)==HIGH){  // condition true when direction of arm is downwards and lower end switch is not pressed
    digitalWrite(IN1, LOW);                                      // Set direction
    digitalWrite(IN2, HIGH);                                     // Set direction
    analogWrite(ENA, 70);                                        // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 100. Default: 70
    digitalWrite(LedDown, HIGH);
  }
  
 } else {
    stopMotorA();
 }
 
 if (prototype2){                              // if prototype 2 is installed as well
  // Conditions for going downwards motor B:
  if(avDistanceB > reactionDistance && millis()-start_up_time > waitTime && digitalRead(endSwitchLowPinB)==HIGH ){    // when filtered distance is large enough AND 'waitTime has passed since going upwards AND the high endstop is not pressed
     actionB = true;                           // boolean to perform actionB is set to true 
     nextDirectionUpB=false;                   // sets direction of motor B
  }

  // What to do when an actionB is to be performed:
 if (actionB ){
  if(nextDirectionUpB && digitalRead(endSwitchHighPinB)==HIGH){  // true when direction of arm should be upwards and higher endstop is not pressed
    digitalWrite(IN4, HIGH);                                     // Set direction
    digitalWrite(IN3, LOW);                                      // Set direction
    analogWrite(ENB, 90);                                        // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 120. Default: 90
  }
  if(!nextDirectionUpB && digitalRead(endSwitchLowPinB)==HIGH){  // condition true when direction of arm is downwards and lower end switch is not pressed
    digitalWrite(IN4, LOW);                                      // Set direction
    digitalWrite(IN3, HIGH);                                     // Set direction
    analogWrite(ENB, 70);                                        // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 100. Default: 70
  }
  
 } else {
    stopMotorB();
 }
 
 }
 
 // For safety 
 if (digitalRead(endSwitchLowPinA)== LOW && digitalRead(endSwitchHighPinA)== LOW){  // endstops are not (properly) connected, may lead to mechanical failure of prototype
     endStopError();                                                                // so shut everything down :)
  }
  if (digitalRead(endSwitchLowPinB)== LOW && digitalRead(endSwitchHighPinB)== LOW && prototype2){  // true when endstops of B are not (properly) connected AND prototype 2 is installed during setup
     endStopError();
  }
 
}

void stopMotorA(){
  digitalWrite(ENA, LOW);          // disable (current through) motor
  digitalWrite(IN1, LOW);          // set direction pins LOW (not sure if required. May save some power
  digitalWrite(IN2, LOW);
  digitalWrite(LedUp, LOW);        // shut off leds
  digitalWrite(LedDown, LOW);
    
}
void stopMotorB(){
  digitalWrite(ENB, LOW);          // disable (current through) motor
  digitalWrite(IN3, LOW);          // set direction pins LOW (not sure if required. May save some power
  digitalWrite(IN4, LOW);
}

void readDistanceA()  {                                      // updates two variables, tempDistance and avDistance
    tempDistanceA=analogRead(sonarANPinA)/2*2.54;            // tempDistanceA is non filtered and instant reading of sensor AN output
    avDistanceA += avFactor*(tempDistanceA - avDistanceA);   // updates the averaged distance with the new input according to the formula: x[+1] = k * x[-1] + (1-k) * x
                                                             // where: k = (1 - avFactor)
                                                             // with: 0 < avFactor < 1  
                                                             // Or something like that. Never mind, just math :) 
                                                             // just keep in mind that a smaller 'avFactor' causes more influence of previous measurements. 
                                                             // This means a slower response when values change quickly, but less sensitivity to unreliable peaks in sensor output
  //Serial.print("A-sensor read succes: ");
  //Serial.println(tempDistanceA);
}

void readDistanceB()  {  
    tempDistanceB=analogRead(sonarANPinB)/2*2.54;
    avDistanceB += avFactor*(tempDistanceB - avDistanceB);
    //Serial.print("B-sensor read succes: ");
    //Serial.println(avDistanceB);
}

void endStopError(){
 stopMotorA();
 stopMotorB();
     
     Serial.println("End stops are not properly installed or stuck");
     Serial.println("Connect/check them and reset Arduino");
     Serial.println("For wiring of end switches, see starting comment in code");
     delay(500);
      cli();                           //stop interrupts
      TIMSK1 |= (0 << OCIE1A);         // disable timer 1 compare interrupt
      sei();                           // allow interrupts, so 'delay()' will work
    for(int i = 0; i <20; i++){        // flicker all leds for some time, just fun :)
     digitalWrite(LedUp, HIGH);
     digitalWrite(LedDown, HIGH);
     digitalWrite(LedDistance, HIGH);
    delay(20);
     digitalWrite(LedUp, LOW);
     digitalWrite(LedDown, LOW);
     digitalWrite(LedDistance, LOW);
     delay(20);
    }
     while(1)                          // remain in this loop forever, until Arduino is reset. Yeah! Infinite loop! Yeah!
     {
       digitalWrite(LedUp, LOW); 
       delay(300);
       digitalWrite(LedUp, HIGH); 
       digitalWrite(LedDown, LOW); 
       delay(300);
       digitalWrite(LedDown, HIGH); 
       digitalWrite(LedDistance, LOW);
       delay(300);
       digitalWrite(LedDistance, HIGH); 
     }
}

