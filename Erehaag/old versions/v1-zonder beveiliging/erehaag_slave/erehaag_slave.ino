#include <IRremote.h>

byte LED_UP = 14;
byte LED_DOWN = 15;
byte LED_STOP = 16;

byte DOWNSENSOR = 2;
byte UPSENSOR = 4;
byte IR_SEND = 3;

int movingToPosition = 0; //0=not moving, 1=up, 2=down
IRsend irsend;

void setup()
{
	Serial.begin(9600);
	pinMode(LED_DOWN, OUTPUT);
	pinMode(LED_UP, OUTPUT);
	pinMode(LED_STOP, OUTPUT);
	pinMode(UPSENSOR, INPUT);
	pinMode(DOWNSENSOR, INPUT);

	irsend.enableIROut(38);
	irsend.mark(0);
}

void loop() {
	MotorLogic();
}

long previousAction = 0;
boolean wasArmMovementAllowed = true;
void MotorLogic()
{
	if (!IsArmMovementAllowed())
	{
		Serial.println("Beweging niet toegestaan");
		MotorBreak();
		return;
	}

	boolean isAtPosition = movingToPosition == GetArmPosition();

	boolean waitingForNewPosition = movingToPosition == 0;
	if (isAtPosition || waitingForNewPosition)
	{
		MotorStop();
		Serial.print("At position, next position is ");
		Serial.println(GetDesiredPosition());
		movingToPosition = GetDesiredPosition();
	}
	else
	{
		Serial.print("moving to position ");
		Serial.println(movingToPosition);
		ConvertPositionToMotorAction(movingToPosition);
	}
}

unsigned long arrivedAtUpPostition;
int GetDesiredPosition()
{
	// get from master
	return 0;
}

int GetArmPosition()
{
	if (digitalRead(UPSENSOR) == HIGH)
	{
		return 1;
	}
	if (digitalRead(DOWNSENSOR) == HIGH)
	{
		return 2;
	}
	return 0;
}

boolean IsArmMovementAllowed()
{
        return true;
}

void ConvertPositionToMotorAction(int position)
{
	switch (position)
	{
	case 1:
		MotorUp();
		break;
	case 2:
		MotorDown();
		break;
	default:
		MotorStop();
		break;
	}
}

void MotorUp()
{
	digitalWrite(LED_STOP, LOW);
	digitalWrite(LED_UP, HIGH);
	digitalWrite(LED_DOWN, LOW);


	//digitalWrite(IN1, HIGH);// Set direction
	//digitalWrite(IN2, LOW);    
	//analogWrite(ENA, 255); // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 100. Default: 70
	//digitalWrite(IN3, LOW);// Set direction
	//digitalWrite(IN4, HIGH);    
	//analogWrite(ENB, 255); // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 100. Default: 70
	////digitalWrite(ENA, LOW);
}

void MotorDown()
{
	digitalWrite(LED_STOP, LOW);
	digitalWrite(LED_UP, LOW);
	digitalWrite(LED_DOWN, HIGH);

	//digitalWrite(IN1, LOW); // Set direction
	//digitalWrite(IN2, HIGH);    
	//analogWrite(ENA, 255); // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 100. Default: 70
	//digitalWrite(IN3, HIGH); // Set direction
	//digitalWrite(IN4, LOW);    
	//analogWrite(ENB, 255); // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 100. Default: 70

}

void MotorStop()
{
	digitalWrite(LED_STOP, LOW);
	digitalWrite(LED_UP, LOW);
	digitalWrite(LED_DOWN, LOW);

	/*digitalWrite(ENA, HIGH);
	digitalWrite(IN1, HIGH);
	digitalWrite(IN2, HIGH);
	digitalWrite(IN3, HIGH);
	digitalWrite(IN4, HIGH);*/
}

void MotorBreak()
{
	digitalWrite(LED_STOP, HIGH);
	digitalWrite(LED_UP, LOW);
	digitalWrite(LED_DOWN, LOW);

	/*digitalWrite(ENA, HIGH);
	digitalWrite(IN1, HIGH);
	digitalWrite(IN2, HIGH);
	digitalWrite(IN3, HIGH);
	digitalWrite(IN4, HIGH);*/
}
