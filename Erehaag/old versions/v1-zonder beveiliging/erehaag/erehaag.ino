#include <CapacitiveSensor.h>

byte LED_UP=14;
byte LED_DOWN=15;
byte LED_STOP=16;

byte DOWNSENSOR = 2;
byte UPSENSOR=3;
byte PIR=4;
byte IR_DETECT=9;

byte MOTOR_UP = 5;
byte MOTOR_DOWN = 6;

int movingToPosition = 0; //0=not moving, 1=up, 2=down

void setup()
{
  Serial.begin(9600);
  pinMode(IR_DETECT, INPUT);
  pinMode(LED_DOWN, OUTPUT);
  pinMode(LED_UP, OUTPUT);
  pinMode(LED_STOP, OUTPUT);
  pinMode(PIR, INPUT);
  pinMode(UPSENSOR, INPUT);
  pinMode(DOWNSENSOR, INPUT);
  pinMode(MOTOR_UP, OUTPUT);
  pinMode(MOTOR_DOWN, OUTPUT);

  digitalWrite(MOTOR_UP, LOW);
  digitalWrite(MOTOR_DOWN, LOW);
}

void loop() {
	MotorLogic();
}

long previousAction = 0;
boolean wasArmMovementAllowed = true;
void MotorLogic()
{
  if(!IsArmMovementAllowed())
  {
      Serial.println("Beweging niet toegestaan");
      MotorBreak();
    return;
  }

    boolean isAtPosition = movingToPosition == GetArmPosition();
   
    boolean waitingForNewPosition = movingToPosition == 0;
    if (isAtPosition || waitingForNewPosition)
    {
        MotorStop();
        Serial.print("At position, next position is ");
        Serial.println(GetDesiredPosition());
        movingToPosition = GetDesiredPosition();
    }
    else
    {
        Serial.print("moving to position ");
        Serial.println(movingToPosition);
        ConvertPositionToMotorAction(movingToPosition);
    }
}

unsigned long arrivedAtUpPostition;
int GetDesiredPosition()
{
	long timeAtUpPosition = millis() - arrivedAtUpPostition;

	if (HumanApproaching())
	{
		arrivedAtUpPostition = millis();
		return 1;
	}
	else if (timeAtUpPosition > 5000)
	{
              Serial.println("Please go down");
           	return 2;
	}
	return 0;
}

int GetArmPosition()
{
   if (digitalRead(UPSENSOR) == HIGH)
   { 
	   return 1;
   }
   if (digitalRead(DOWNSENSOR) == HIGH)
   {
	   return 2;
   }
   return 0;
}

CapacitiveSensor   cs_4_2 = CapacitiveSensor(6, 7);        // 10M resistor between pins 4 (send > 10K) & 2 (recieve = 1k), pin 2 is sensor pin, add a wire and or foil if desired
boolean deviceTouched = false;
boolean ArmIsTouched()
{
	long touchValue = cs_4_2.capacitiveSensor(30);

	if (touchValue > 10)
	{
		if (!deviceTouched)
		{
			Serial.print("Device touched, with pressure: ");
			Serial.println(touchValue);
	        }
		deviceTouched = true;
	}
	else
	{
		if (deviceTouched)
		{
			Serial.println("Released");
		}
		deviceTouched = false;
        }
}

boolean HumanApproaching()
{
    int pirVal = digitalRead(PIR);

  if(pirVal == 1)
  { 
    Serial.println("Motion Detected"); 
    return true;  
  }
  return false;
}
 
boolean IsArmMovementAllowed()
{
	return true;
	//return !IsObstacleDetected();
        //return !ArmIsTouched();
    //return !IsObstacleDetected() && !ArmIsTouched();
}

boolean IsObstacleDetected()
{
	boolean hasSignal = !digitalRead(IR_DETECT);
	return !hasSignal;
}

void ConvertPositionToMotorAction(int position)
{
	switch (position)
	{
		case 1:
			MotorUp();
			break;
		case 2:
			MotorDown();
			break;
		default:
			MotorStop();
			break;
	}
}

void MotorUp()
{
   digitalWrite(LED_STOP, LOW);
   digitalWrite(LED_UP, HIGH);
   digitalWrite(LED_DOWN, LOW);
 
   digitalWrite(MOTOR_UP, HIGH);
   digitalWrite(MOTOR_DOWN, LOW);
}

void MotorDown()
{ 
   digitalWrite(LED_STOP, LOW);
   digitalWrite(LED_UP, LOW);
   digitalWrite(LED_DOWN, HIGH);

   digitalWrite(MOTOR_UP, LOW);
   digitalWrite(MOTOR_DOWN, HIGH);
}

void MotorStop()
{
   digitalWrite(LED_STOP, LOW);
   digitalWrite(LED_UP, LOW);
   digitalWrite(LED_DOWN, LOW);

   digitalWrite(MOTOR_UP, LOW);
   digitalWrite(MOTOR_DOWN, LOW);
}

void MotorBreak()
{
	digitalWrite(LED_STOP, HIGH);
	digitalWrite(LED_UP, LOW);
	digitalWrite(LED_DOWN, LOW);

	digitalWrite(MOTOR_UP, LOW);
	digitalWrite(MOTOR_DOWN, LOW);
}
