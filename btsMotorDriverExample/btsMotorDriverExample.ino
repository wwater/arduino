/*
 /*
 # This Sample code is for testing the DC Motor Driver 2x15A_lite module.
  
 # Editor : Phoebe
 # Date   : 2012.11.6
 # Ver    : 0.1
 # Product: DC Motor Driver 2x15A_lite
 # SKU    : DRI0018
  
 # Description:     
 # Drive 2 motors with this DC Motor Driver module  
  
 # Hardwares:
 1. Arduino UNO
 2. DC Motor Driver 2x15A_lite  
 3. DC motors x2
  
 #Steps:
 1.Connect the M1_PWM & M2_PWM to UNO digital 5 & 6
 2.Connect the M1_EN & M2_EN to UNO digital 4 & 7
 3.Connect +5V & GND to UNO 5V & GND
  
 # Function for current sense and diagnosis,if you want to use
 please connect the IS pins to Arduino
 Connect LA_IS and RA_IS to UNO digital 2 at the same time
 Connect LB_IS and RB_IS to UNO digital 3 at the same time
 */
 
int E1 = 9;     //M1 Speed Control
int Error1 = 16;
int Error2 = 17;
int M1 = 18;     //M1 Direction Control
int M2 = 19;     //M1 Direction Control
int ErrorLed = 6;
int counter=0;
 
void setup(void) 
{ 
  int i;
  pinMode(E1, OUTPUT);
  pinMode(M1, OUTPUT);
  pinMode(M2, OUTPUT);
  pinMode(ErrorLed, OUTPUT);
  pinMode(Error1, INPUT);
  pinMode(Error2, INPUT);
  
  Serial.begin(19200);      //Set Baud Rate
  Serial.println("Run keyboard control");
  digitalWrite(E1,LOW);   
  // Set pin 9's PWM frequency to 3906 Hz (31250/8 = 3906)
  // Note that the base frequency for pins 3, 9, 10, and 11 is 31250 Hz
  setPwmFrequency(9, 1);
  
  
  digitalWrite(ErrorLed, HIGH);
  delay(500);
  digitalWrite(ErrorLed, LOW);
  delay(500);
  digitalWrite(ErrorLed, HIGH);
  delay(500);
  digitalWrite(ErrorLed, LOW);
  delay(500);
  
}  
 
void stop(void)                    //Stop
{
  Serial.println("Stop");
  digitalWrite(E1,0); 
  digitalWrite(M1,LOW);   
  digitalWrite(M2,LOW);    
}   
void advance(char a,char b)          //Move forward
{
  Serial.println("Forward");
  analogWrite (E1,a);      //PWM Speed Control
  digitalWrite(M1,HIGH);    
  digitalWrite(M2,LOW);
}  
void back_off (char a,char b)          //Move backward
{
  Serial.println("Backwards");
  analogWrite (E1,a);
  digitalWrite(M1,LOW);   
  digitalWrite(M2,HIGH);
}
void current_sense()                  // current sense and diagnosis
{
  int val1=digitalRead(2);            
  int val2=digitalRead(3);
  if(val1==HIGH || val2==HIGH){
    counter++;
    if(counter==3){
      counter=0;
      digitalWrite(ErrorLed, HIGH);
      Serial.println("Warning");
    }  
  }
  digitalWrite(ErrorLed, LOW); 
}
 

  int motorSpeed = 180; 
  // timer is running on 31kHz, but driver can handle max 25 kHz. Motor is quiet with high torq, so no high noise. 
  // Run on duty cycle of max 200, to prevent overheating of the driver.
  
void loop(void) 
{
  
  static unsigned long timePoint = 0;    // current sense and diagnosis,if you want to use this 
   if(millis() - timePoint > 1000){       //function,please show it & don't forget to connect the IS pins to Arduino                                             
   current_sense();
   timePoint = millis();
   }
   

  
  if(Serial.available()){
    char val = Serial.read();
    if(val != -1)
    {
      switch(val)
      {
      case 'w'://Move Forward
        advance (motorSpeed,motorSpeed);   //move forward in max speed
        break;
      case 's'://Move Backward
        back_off (motorSpeed,motorSpeed);   //move back in max speed
        break;
      case 'z':
        Serial.println("Hello");
        break;
      case 'x':
        stop();
        break;
      case 'q':
         motorSpeed++;
         analogWrite (E1,motorSpeed);
         Serial.println(motorSpeed);
         break;
       case 'e':
         motorSpeed--;
         analogWrite (E1,motorSpeed);
         Serial.println(motorSpeed);
         break;
      }
    }
    else stop();  
  }
 
}

/**
 * Divides a given PWM pin frequency by a divisor.
 * 
 * The resulting frequency is equal to the base frequency divided by
 * the given divisor:
 *   - Base frequencies:
 *      o The base frequency for pins 3, 9, 10, and 11 is 31250 Hz.
 *      o The base frequency for pins 5 and 6 is 62500 Hz.
 *   - Divisors:
 *      o The divisors available on pins 5, 6, 9 and 10 are: 1, 8, 64,
 *        256, and 1024.
 *      o The divisors available on pins 3 and 11 are: 1, 8, 32, 64,
 *        128, 256, and 1024.
 * 
 * PWM frequencies are tied together in pairs of pins. If one in a
 * pair is changed, the other is also changed to match:
 *   - Pins 5 and 6 are paired on timer0
 *   - Pins 9 and 10 are paired on timer1
 *   - Pins 3 and 11 are paired on timer2
 * 
 * Note that this function will have side effects on anything else
 * that uses timers:
 *   - Changes on pins 3, 5, 6, or 11 may cause the delay() and
 *     millis() functions to stop working. Other timing-related
 *     functions may also be affected.
 *   - Changes on pins 9 or 10 will cause the Servo library to function
 *     incorrectly.
 * 
 * Thanks to macegr of the Arduino forums for his documentation of the
 * PWM frequency divisors. His post can be viewed at:
 *   http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1235060559/0#4
 */
void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}
