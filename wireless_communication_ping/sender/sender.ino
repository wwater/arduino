// http://playground.arduino.cc/InterfacingWithHardware/Nrf24L01
// http://www.elecfreaks.com/wiki/index.php?title=2.4G_Wireless_nRF24L01p
// viewed from print side, one is in corner
// 1 GND          2 VCC (3.3V, 5 will burn!)
// 3 CE (D8)	  4 CSN (D9)
// 5 SCK (D13)    6 MOSI (D11)
// 7 MISO (D12)   9 IRQ (-)

// this is the SENDER code
#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>

void setup(){
  Serial.begin(9600);
  Mirf.csnPin = 8; //Test with 9
  Mirf.cePin = 7;
  Mirf.spi = &MirfHardwareSpi;
  Mirf.init();
  
  Mirf.setRADDR((byte *)"clie1");
  Mirf.payload = sizeof(unsigned long);
  
  Mirf.config();
  
  Serial.println("Beginning ... "); 
}

void loop(){
  unsigned long time = millis();
  unsigned long timeReceived;
  
  Mirf.setTADDR((byte *)"serv1");
  
  Mirf.send((byte *)&time);
  Serial.print(Mirf.getStatus());
  while(Mirf.isSending()){
   
  }
    Serial.print(Mirf.getStatus());
    // Return true if still trying to send. If the chip is still in transmit mode then this method will return the chip to receive mode.
    Serial.println("Started sending");
    delay(10);
    while(!Mirf.dataReady()){
      //Serial.println("Waiting");
      if ( ( millis() - time ) > 1000 ) {
        Serial.println("Timeout on response from server!");
        return;
      }
  }
  
  Mirf.getData((byte *) &timeReceived);
  
  Serial.print("time received: ");
  Serial.print(timeReceived);
  Serial.print("ping: ");
  Serial.println((millis() - timeReceived));
  
  delay(1000);
} 
  
  
  

