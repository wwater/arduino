// http://playground.arduino.cc/InterfacingWithHardware/Nrf24L01
// http://www.elecfreaks.com/wiki/index.php?title=2.4G_Wireless_nRF24L01p
// viewed from print side, one is in corner
// 1 GND          2 VCC (3.3V, 5 will burn!)
// 3 CE (D8)	  4 CSN (D7)
// 5 SCK (D13)    6 MOSI (D11)
// 7 MISO (D12)   9 IRQ (-)

#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>

void setup()
{
  Serial.begin(9600);
  Mirf.spi = &MirfHardwareSpi;
  Mirf.csnPin = 8; // test with 9
  Mirf.cePin = 7;
  
  Mirf.init();
  Mirf.setRADDR((byte *)"serv1");
  Mirf.payload = sizeof(unsigned long);
  
  Mirf.config();
  
  Serial.println("Listening..."); 
}

unsigned long timestamp;
void loop()
{
  byte data[Mirf.payload];
    
  if(!Mirf.isSending() && Mirf.dataReady()){
    Serial.println("Got packet");
    Mirf.getData((byte *)&timestamp);
    Mirf.setTADDR((byte *)"clie1");   
    Mirf.send((byte*)timestamp);
    Serial.print(timestamp);
    Serial.println("Reply sent.");
  }
}

