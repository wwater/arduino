
// - uses l298n dual H-bridge driver for dc motor control (See for example: http://www.ebay.com/itm/290899895209 or google pictures on "L298n module Arduino")

byte ENA=5;                   // pin connected to Enable A on motor driver card (to turn motor on or off (or PWM))
byte IN1=8;                   // pin connected to In 1 on motor driver card (to specify direction)
byte IN2=9;                   // pin connected to In 2 on motor driver card (to specify direction)

byte ENB = 6;                // see ENA
byte IN3 =10;
byte IN4 = 11;


void setup()
{
  pinMode(ENA, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
 
  byte ENA2 = 11;
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);    // goed initialiseren, knippert by startup 
}

void loop()
{
    digitalWrite(IN1, HIGH);                                     // Set direction
    digitalWrite(IN2, LOW);                                      // Set direction
    analogWrite(ENA, 70);                                        // Enable motor turning with given PWM duty cycle (0-255). When connected to 12V: not advisable to exceed 120. Default: 90
}
