/* YourDuino.com Example Software Sketch
   Small Stepper Motor and Driver V1.3 11/30/2013
   http://arduino-direct.com/sunshop/index.php?l=product_detail&p=126
   Shows 4-step sequence, Then 1/2 turn and back different speeds
   terry@yourduino.com */
#include <Stepper.h>

/*-----( Declare Constants, Pin Numbers )-----*/
//---( Number of steps per revolution of INTERNAL motor in 4-step mode )---
#define STEPS_PER_MOTOR_REVOLUTION 32
//---( Steps per OUTPUT SHAFT of gear reduction )---
#define STEPS_PER_OUTPUT_REVOLUTION 32 * 64  //2048  

//The pin connections need to be 4 pins connected
// to Motor Driver In1, In2, In3, In4  and then the pins entered
// here in the sequence 1-3-2-4 for proper sequencing
Stepper small_stepper(STEPS_PER_MOTOR_REVOLUTION, 8, 10, 9, 11);

/*----- Sonar -----*/
#define echoPin 2 // Echo Pin
#define trigPin 4 // Trigger Pin
#define LEDPin 13 // Onboard LED

int rotationDirection = 1;

void setup()  
{
   Serial.begin (9600);
   Serial.println("Boot ready");
   pinMode(trigPin, OUTPUT);
   pinMode(echoPin, INPUT);
   pinMode(LEDPin, OUTPUT);
   
   digitalWrite(LEDPin, LOW);
}

void loop()   
{

    digitalWrite(LEDPin, HIGH); 
  if(IsInRange(0, 30))
  {
     EnableLed(true);
     SetRotationDirectionToLeft();  
     Rotate();
  }
  else
  {
     EnableLed(false);
     delay(10);
  }
}

void EnableLed(boolean enable)
{
   if(enable)
  {
    digitalWrite(LEDPin, HIGH); 
  }
   else
  {
    digitalWrite(LEDPin, LOW);
  } 
}

void SetRotationDirectionToLeft()
{
  rotationDirection = -1;
}

void SetRotationDirectionToRight()
{
  rotationDirection = 1;
}

void ToggleRotationDirection()
{
    rotationDirection = rotationDirection * -1;
}

void Rotate()
{
      int steps2Take  =  STEPS_PER_OUTPUT_REVOLUTION / 40;  // Rotate CW 1/2 turn
      steps2Take = steps2Take * rotationDirection;
      small_stepper.setSpeed(700);   
      small_stepper.step(steps2Take);
}

long GetDistanceInCm()
{
   digitalWrite(trigPin, LOW); 
   delayMicroseconds(2); 
  
   digitalWrite(trigPin, HIGH);
   delayMicroseconds(10); 
   
   digitalWrite(trigPin, LOW);
   long duration = pulseIn(echoPin, HIGH);
   
   //Calculate the distance (in cm) based on the speed of sound.
   long distance = duration/58.2;
   
   Serial.println(distance);
}

bool IsInRange(int from, int to)
{
   long distance = GetDistanceInCm();
   return distance >= from && distance <= to;
}

