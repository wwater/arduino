/*
 * Switch test program
 */
 
 /*
Changing the PWM frequency on Pins 11 and 3
Setting	Divisor	Frequency
0x01	1	31250  //should not be able to hear this though the frequency is a little high for the l298
0x02	8	3906.25
0x03	32	976.5625
0x04	64	488.28125 // default
0x05	128	244.140625
0x06	256	122.0703125
0x07	1024	30.517578125 //here is also good..in theroy but may be a little to low for the dc motor 
*/


int potx = A0; // analog pin 0, connected to VRx on the joystick board
int poty = A1; // analog pin 1, connected to VRy on the joystick board
// The joystick board 5 volts needs to be connected to the Arduino 5 volts pin
// The joystick board ground needs to be connected to an Arduino ground pin

int motor1 = 3; //Digital pin 3, connected to Enable pin A(ENA) on the L298N
int motor2 = 11; //Digital pin 11, connected to Enable pin B(ENB) on the L298N

int IN1 = 4; //Digital pin 4, connected to IN1 on the L298N
int IN2 = 5; //Digital pin 5, connected to IN2 on the L298N
int IN3 = 6; //Digital pin 6, connected to IN3 on the L298N
int IN4 = 7; //Digital pin 7, connected to IN4 on the L298N
// The L298N ground needs to be connected to an Arduino ground pin.
// In the L298N one DC motor terminals will be connected to OUT1 and OUT2 and the second DC motor to OUT3 and OUT4
// polarity in the motors is determined by the direction that you want for pan and tilt. 

int value = 6; //4 being the default giving  488.28125hz

int CenterX = 0;
int CenterY = 0;

void setup()                    // run once, when the sketch starts
{
  Serial.begin(9600);           // set up Serial library at 9600 bps
  pinMode(potx, INPUT); 
  pinMode(poty, INPUT); 
  pinMode(motor1, OUTPUT);
  pinMode(motor2, OUTPUT);
  pinMode (IN1, OUTPUT);
  pinMode (IN2, OUTPUT);
  pinMode (IN3, OUTPUT);
  pinMode (IN4, OUTPUT);
  TCCR2B = (TCCR2B & 0xF8) | value;// change the value of the (Timer 2 pins 11, 3?) register to a value ranging between 1 - 7
  
  CenterX = analogRead(potx); // Get the center x value in the joystick
  CenterY = analogRead(poty); // Get the center y value in the joystick

}

int getPotX() {
  int v;
  v = analogRead(potx);
  return v;
}
int getPotY() {
  int v;
  v = analogRead(poty);
  return v;
}

int moveMotorX() {
  
  int ValueX = getPotX();
  if( ValueX < CenterX ){
    digitalWrite (IN1, HIGH);
    digitalWrite (IN2, LOW);
    ValueX = CenterX - ValueX;
    ValueX /= 4;
    ValueX = max(ValueX, 0);
    ValueX = min(ValueX, 255);
    analogWrite(motor1, ValueX);
  }
  else{
    digitalWrite (IN1, LOW);
    digitalWrite (IN2, HIGH);
    ValueX = ValueX - CenterX;
    ValueX /= 4;
    ValueX = max(ValueX, 0);
    ValueX = min(ValueX, 255);
    analogWrite(motor1, ValueX);
  }
}
int moveMotorY() {
  
  int ValueY = getPotY();
  if( ValueY < CenterY ){
    digitalWrite (IN3, HIGH);
    digitalWrite (IN4, LOW);
    ValueY = CenterY - ValueY;
    ValueY /= 4;
    ValueY = max(ValueY, 0);
    ValueY = min(ValueY, 255);
    analogWrite(motor2, ValueY);
  }
  else{
    digitalWrite (IN3, LOW);
    digitalWrite (IN4, HIGH);
    ValueY = ValueY - CenterY;
    ValueY /= 4;
    ValueY = max(ValueY, 0);
    ValueY = min(ValueY, 255);
    analogWrite(motor2, ValueY);
  }
}

void loop()                     // run over and over again
{
  moveMotorX();
  moveMotorY();
}
