const int pir = A0;  // Analog input pin that the potentiometer is attached to

int sensorValue = 0;        // value read from the pot

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600); 
  pinMode(pir, INPUT);
}

void loop() {
  // read the analog in value:
  sensorValue = analogRead(pir);    
  Serial.println(sensorValue);  
  delay(100);                     
}
