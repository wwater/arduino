// Uses a PIR sensor to detect movement, buzzes a buzzer
// more info here: http://blog.makezine.com/projects/pir-sensor-arduino-alarm/
// email me, John Park, at jp@jpixl.net
// based upon:
// PIR sensor tester by Limor Fried of Adafruit
// tone code by michael@thegrebs.com

 
int ledPin = 13;                // choose the pin for the LED
int inputPin = 2;               // choose the input pin (for PIR sensor)
int pirState = LOW;
void setup() {
  pinMode(ledPin, OUTPUT);      // declare LED as output
  pinMode(inputPin, INPUT);     // declare sensor as input
  Serial.begin(9600);
}

void loop(){
  int val = digitalRead(inputPin);  // read input value
  Serial.println(val);
  boolean humanDetected = val == HIGH;
  
  EnableLed(humanDetected); 
  if(humanDetected)
  {
     Serial.println("Human detected");
  }
  delay(10);
}

void EnableLed(boolean enable)
{
   if(enable)
  {
    digitalWrite(ledPin, HIGH); 
  }
   else
  {
    digitalWrite(ledPin, LOW);
  } 
}

