//http://bildr.org/2011/06/pir_arduino/

// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
boolean isHumanDetected = true;
#define PIR 8  
#define RELAY1  13  
int val = 0;

void setup() 
{
  Serial.begin(9600);
  pinMode(PIR, INPUT); 
  pinMode(RELAY1, OUTPUT);     
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Scanning for humans ...");
}

void loop()
{
    val = digitalRead(PIR);
    Serial.println(val);
    HumanDetected(val == HIGH);
    Serial.println(val);
    //delay(10);
}

void HumanDetected(boolean isDetected)
{
  if(isDetected != isHumanDetected)
  {
    isHumanDetected = isDetected;
    if(isHumanDetected)
    {
      lcd.clear();
      lcd.println("Human detected");
      digitalWrite(RELAY1,HIGH);  
    }
    else
    {
       lcd.clear();  
       lcd.println("All ok"); 
       digitalWrite(RELAY1,LOW);  
    }
  } 
}
