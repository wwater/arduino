#include <Arduino.h>

void setup();
void loop();

void setup()
{
	Serial.begin(9600);
	pinMode(13, OUTPUT);
	Serial.println("Listening...");
}

int i;
void loop()
{
	digitalWrite(13, HIGH);
	delay(500);
	digitalWrite(13, LOW);
	delay(500);
	i++;
	Serial.print("Got packet " + String(i));
}