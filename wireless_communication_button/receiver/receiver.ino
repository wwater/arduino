// http://playground.arduino.cc/InterfacingWithHardware/Nrf24L01
// http://www.elecfreaks.com/wiki/index.php?title=2.4G_Wireless_nRF24L01p
// viewed from print side, one is in corner
// 1 GND          2 VCC (3.3V, 5 will burn!)
// 3 CE (D8)	  4 CSN (D9)
// 5 SCK (D13)    6 MOSI (D11)
// 7 MISO (D12)   9 IRQ (-)

#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>

byte ledPin = 4;

void setup()
{
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
  Mirf.spi = &MirfHardwareSpi;
  Mirf.csnPin = 9;
  Mirf.cePin = 8;
  Mirf.init();
  Mirf.setRADDR((byte *)"m1");
  Mirf.setTADDR((byte *)"s1");
  Mirf.payload = 1;
  Mirf.config();

  Serial.println("Listening..."); 
  
  blinkLed(183);
}

void loop()
{
    byte data[Mirf.payload];
    
  if(!Mirf.isSending() && Mirf.dataReady()){
    Serial.print("Got packet ");
    Mirf.getData((byte *)data);

	if (sizeof(data) > 0)
	{
            blinkLed(data[0]);
	}
  }
}

void blinkLed(int data)
{
  int motorAction = (convertDataToMotorAction(data) + 1) * 2;
  int i; 
  for(i=0;i<=motorAction;i++)
  {
     digitalWrite(ledPin, HIGH);
     delay(100);
     digitalWrite(ledPin, LOW); 
      delay(100);
  }
  delay(500);
}

int convertDataToMotorAction(int data)
{
  switch (data)
	{
		case 0:
			return 0;
		case 183:
			return 1;
		case 219:
			return 2;
		default:
			return -1;
	}
}
