// http://playground.arduino.cc/InterfacingWithHardware/Nrf24L01
// http://www.elecfreaks.com/wiki/index.php?title=2.4G_Wireless_nRF24L01p
// viewed from print side, one is in corner
// 1 GND          2 VCC (3.3V, 5 will burn!)
// 3 CE (D8)	  4 CSN (D9)
// 5 SCK (D13)    6 MOSI (D11)
// 7 MISO (D12)   9 IRQ (-)

// this is the SENDER code
#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>

byte DOWNSENSOR = 2;
byte UPSENSOR = 3;

void setup(){
  Serial.begin(9600);
  Mirf.csnPin = 9;
  Mirf.cePin = 8;
  Mirf.spi = &MirfHardwareSpi;
  Mirf.init();
  Mirf.setRADDR((byte *)"clie1");
  Mirf.payload = 1;
  Mirf.config();
  
  pinMode(UPSENSOR, INPUT);
  pinMode(DOWNSENSOR, INPUT);
  Mirf.setTADDR((byte *)"serv1");
  Serial.println("Beginning ... "); 
}

int previousArmPosition = 0;

void loop(){
  
	if (previousArmPosition != GetArmPosition())
	{
		previousArmPosition == GetArmPosition();
		
		Mirf.send((byte *)GetArmPosition()); 
		while (Mirf.isSending()){}
	}
} 

int GetArmPosition()
{
	if (digitalRead(UPSENSOR) == HIGH)
	{
		return 1;
	}
	if (digitalRead(DOWNSENSOR) == HIGH)
	{
		return 2;
	}
	return 3;
}
  
  
  

