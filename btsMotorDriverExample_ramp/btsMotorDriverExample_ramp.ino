#include "Timer.h"
#include <PWM.h>
 
Timer t; 

int E1 = 9;     //M1 Speed Control
int M1 = 7;     //M1 Direction Control
int M2 = 8;     //M1 Direction Control
int LA_IS = 4;
int RA_IS = 5;
int counter=0;

int motorSpeedMin = 0;
int motorSpeedMax = 255;
int motorSpeedStep = 5;
int motorSpeedUpdateFrequency = 10; // time between a change in speed, in ms. 

void setup(void) 
{ 
  pinMode(E1, OUTPUT);
  pinMode(M1, OUTPUT);
  pinMode(M2, OUTPUT);
  pinMode(LA_IS,INPUT);
  pinMode(RA_IS,INPUT);
  pinMode(13, OUTPUT);
  Serial.begin(9200);
  Serial.println("Run keyboard control");
  digitalWrite(E1,LOW);   
  
  t.every(motorSpeedUpdateFrequency, updateMotorSpeed); // set timer to update moter at given interval
  
  //initialize all timers except for 0, to save time keeping functions
  InitTimersSafe(); 

  // set pwm frequenty to a proper value for dc motor driver (20000 hz). Max 25000 hz is allowed by the driver
  bool success = SetPinFrequencySafe(E1, 20000); 
  if(!success)
  {
    Serial.println("pwm frequention not valid");
  }
} 
  
void loop(void) 
{
   t.update(); // update motor driver
   
   // check if motor driver is ok.
  static unsigned long timePoint = 0;
   if(millis() - timePoint > 1000){                                             
     current_sense();
     timePoint = millis();
   }
    
    // read command from serial, so a motor action will be done. 
  if(Serial.available()){
    char val = Serial.read();
    if(val != -1)
    {
      switch(val)
      {
      case 'w':
        up ();   
        break;
      case 's':
        down ();  
        break;
      case 'x':
        stop();
        break;
      }
    }
    else stop();  
  }
} 

int motorDirection = 0; //0=stop, 1=up, 2=down
int motorDirectionPrevious = 0;
int motorSpeedCurrent = 0;
boolean switchingMotorDirection = false;

void updateMotorSpeed()
{
    Serial.print(motorDirection);
    Serial.print(" - speed: ");
    Serial.println(motorSpeedCurrent);
    
    if(motorDirection == 0)
    {
        motorSpeedCurrent = 0;
        return;
    }
    
    if(motorSpeedCurrent == 0)
    {
      switchingMotorDirection = false;
      motorDirectionPrevious = motorDirection;
    }
    
    if(switchingMotorDirection)
    {
        stopRamped();
        return;
    }
    
    if(motorDirection != motorDirectionPrevious)
    {
        switchingMotorDirection = true;
        return;
    }  
    
    if(motorDirection == 1)
    {
      digitalWrite(M1,HIGH);   
      digitalWrite(M2,LOW); 
    }
    else if(motorDirection == 2)
    {
      digitalWrite(M1,LOW);   
      digitalWrite(M2,HIGH);
    }
    startRamped();
}

void startRamped()
{
      if(motorSpeedCurrent < motorSpeedMin)
      {
        motorSpeedCurrent = motorSpeedMin;
      }
         // from min to max in stes
         int newMotorSpeed = motorSpeedCurrent + motorSpeedStep;
         if(newMotorSpeed <= motorSpeedMax)
         { 
            pwmWrite (E1,newMotorSpeed);
            motorSpeedCurrent = newMotorSpeed;
         } 
}

void stopRamped()
{
       // from current to min speed to zero
       int newMotorSpeed = motorSpeedCurrent - motorSpeedStep;
       if(newMotorSpeed < motorSpeedMin)
       { 
          newMotorSpeed = 0;
       }
       
       pwmWrite (E1,newMotorSpeed);
       motorSpeedCurrent = newMotorSpeed;
}

void stop(void)
{
  motorDirection = 0;
  digitalWrite(E1,0); 
  digitalWrite(M1,LOW);   
  digitalWrite(M2,LOW);   
}   
void up()
{
  motorDirection = 1; 
}  
void down ()
{ 
  motorDirection = 2;
}

void current_sense()
{
  int val1=digitalRead(LA_IS);            
  int val2=digitalRead(RA_IS);
  if(val1==HIGH || val2==HIGH){
    counter++;
    if(counter==3){
      counter=0;
      Serial.println("Warning");
    }  
  } 
}
