/* 
  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)
 http://www.arduino.cc/en/Tutorial/LiquidCrystal

https://code.google.com/p/arduino-new-ping/wiki/Ping_Event_Timer_Sketch

 */
#include <LiquidCrystal.h>
#include <NewPing.h>
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

#define ECHO_PIN 6
#define TRIGGER_PIN 7 
#define MAX_DISTANCE 100 

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

unsigned int pingSpeed = 50; // How frequently are we going to send out a ping (in milliseconds). 50ms would be 20 times a second.
unsigned long pingTimer;     // Holds the next ping time.

long humanDetectedAt;
long humanDetectedAtDistance; 
long previousPrint;
boolean lcdAllClear = true;

void setup() {
   Serial.begin(115200); 
   pingTimer = millis();
  
   Serial.println("Boot ready");
   lcd.begin(16, 2);
   lcd.print("I am ready...");
}

void loop() {
    // Notice how there's no delays in this sketch to allow you to do other processing in-line while doing distance pings.
  if (millis() >= pingTimer) {   // pingSpeed milliseconds since last ping, do another ping.
    pingTimer += pingSpeed;      // Set the next ping time.
    sonar.ping_timer(echoCheck); // Send out the ping, calls "echoCheck" function every 24uS where you can check the ping status.
  }
  
  boolean humanDetected = HumanDetected();
    

  
  delay(1000);
}

void echoCheck() { // Timer2 interrupt calls this function every 24uS where you can check the ping status.
  if (sonar.check_timer()) { // This is how you check to see if the ping was received.
      humanDetectedAtDistance = sonar.ping_cm();
      humanDetectedAt = millis();
  }
  
  ShowLcdMessage(HumanDetected());
}

boolean HumanDetected()
{       
    long waitTime = 2000;
    boolean isOldDetection = humanDetectedAt + waitTime < millis();
    Serial.println(isOldDetection);
    return IsInRange(humanDetectedAtDistance, 1, 20) && !isOldDetection;    
}

bool IsInRange(long distance, int from, int to)
{
  return distance >= from && distance <= to;  
}

void ShowLcdMessage(humanDetected)
{
   Serial.print("human detected: "); Serial.println(humanDetected);
  if(humanDetected && lcdAllClear)
  {
    lcdAllClear = false; 
    lcd.setCursor(0,0);
     lcd.println("Human detected");
     lcd.setCursor(0,1);
     lcd.print("Distance: ");
     lcd.print(humanDetectedAtDistance,10);
     lcd.print("cm");
  }  
  if(!humanDetected && !lcdAllClear)
  {
    lcdAllClear = true;
    lcd.clear();
    lcd.println("All clear");
  } 
}


