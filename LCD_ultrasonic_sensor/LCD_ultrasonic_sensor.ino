/*
  LiquidCrystal Library - Hello World
 
 Demonstrates the use a 16x2 LCD display.  The LiquidCrystal
 library works with all LCD displays that are compatible with the 
 Hitachi HD44780 driver. There are many of them out there, and you
 can usually tell them by the 16-pin interface.
 
 This sketch prints "Hello World!" to the LCD
 and shows the time.
 
  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)
 
 Library originally added 18 Apr 2008
 by David A. Mellis
 library modified 5 Jul 2009
 by Limor Fried (http://www.ladyada.net)
 example added 9 Jul 2009
 by Tom Igoe
 modified 22 Nov 2010
 by Tom Igoe
 
 This example code is in the public domain.

 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 */

// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

#define echoPin 6 // Echo Pin
#define trigPin 7 // Trigger Pin

void setup() {
   pinMode(trigPin, OUTPUT);
   pinMode(echoPin, INPUT);
   Serial.begin (9600);
   Serial.println("Boot ready");
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("I am ready...");
}

void loop() {
    long distance = GetDistanceInCm();
    boolean humanDetected = IsInRange(distance, 1, 200);
    
    lcd.clear();
    if(humanDetected)
    {
        lcd.setCursor(0,0);
        lcd.println("Human detected");
        lcd.setCursor(0,1);
        lcd.print("Distance: ");
        lcd.print(distance,10);
        lcd.print("cm");
    }
    else
    {
       lcd.print("All clear");
    }
}

long GetDistanceInCm()
{
   digitalWrite(trigPin, LOW); 
   delayMicroseconds(2); 
  
   digitalWrite(trigPin, HIGH);
   delayMicroseconds(10); 
   
   digitalWrite(trigPin, LOW);
   long duration = pulseIn(echoPin, HIGH);
   
   //Calculate the distance (in cm) based on the speed of sound.
   long distance = duration/58.2;
   
   Serial.println(distance);
   return distance;
}

bool IsInRange(long distance, int from, int to)
{
  return distance >= from && distance <= to;  
}

bool IsInRange(int from, int to)
{
   long distance = GetDistanceInCm();
   return distance >= from && distance <= to;
}


