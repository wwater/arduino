/*
 /*
 # This Sample code is for testing the DC Motor Driver 2x15A_lite module.
  
 # Editor : Phoebe
 # Date   : 2012.11.6
 # Ver    : 0.1
 # Product: DC Motor Driver 2x15A_lite
 # SKU    : DRI0018
  
 # Description:     
 # Drive 2 motors with this DC Motor Driver module  
  
 # Hardwares:
 1. Arduino UNO
 2. DC Motor Driver 2x15A_lite  
 3. DC motors x2
  
 #Steps:
 1.Connect the M1_PWM & M2_PWM to UNO digital 5 & 6
 2.Connect the M1_EN & M2_EN to UNO digital 4 & 7
 3.Connect +5V & GND to UNO 5V & GND
  
 # Function for current sense and diagnosis,if you want to use
 please connect the IS pins to Arduino
 Connect LA_IS and RA_IS to UNO digital 2 at the same time
 Connect LB_IS and RB_IS to UNO digital 3 at the same time
 */
 
int E1 = 9;     //M1 Speed Control
int M1 = 8;     //M1 Direction Control
int M2 = 7;     //M1 Direction Control
int counter=0;
 
void stop(void)                    //Stop
{
  digitalWrite(E1,0); 
  digitalWrite(M1,LOW);   
  digitalWrite(M2,LOW);    
}   
void advance(char a,char b)          //Move forward
{
  analogWrite (E1,a);      //PWM Speed Control
  digitalWrite(M1,HIGH);    
  digitalWrite(M2,LOW);
}  
void back_off (char a,char b)          //Move backward
{
  analogWrite (E1,a);
  digitalWrite(M1,LOW);   
  digitalWrite(M2,HIGH);
}
void current_sense()                  // current sense and diagnosis
{
  int val1=digitalRead(2);            
  int val2=digitalRead(3);
  if(val1==HIGH || val2==HIGH){
    counter++;
    if(counter==3){
      counter=0;
      Serial.println("Warning");
    }  
  } 
}


int motorSpeed = 180; 
 
void setup(void) 
{ 
  int i;
  pinMode(E1, OUTPUT);
  pinMode(M1, OUTPUT);
  pinMode(M2, OUTPUT);
  Serial.begin(19200);      //Set Baud Rate
  Serial.println("Run keyboard control");
  digitalWrite(E1,LOW);   
  // Set pin 9's PWM frequency to 3906 Hz (31250/8 = 3906)
  // Note that the base frequency for pins 3, 9, 10, and 11 is 31250 Hz
  //setPwmFrequency(9, 1);
  
  // configure hardware timer1 to generate a fast PWM on OC2B (Arduino digital pin 3)
  // set pin high on overflow, clear on compare match with OCR2B
  TCCR2A = 0x23;
  TCCR2B = 0x0C;  // select timer2 clock as 16 MHz I/O clock / 64 = 250 kHz
  OCR2A = 249;  // top/overflow value is 249 => produces a 1000 Hz PWM
  pinMode(9, OUTPUT);  // enable the PWM output (you now have a PWM signal on digital pin 9 and 10)
  OCR2B = motorSpeed;  // set the PWM to 50% duty cycle (0 to 250) (van 100 - 200 - naar 255)

  pinMode(2,INPUT);
  pinMode(3,INPUT);
} 
  
void loop(void) 
{
  
  static unsigned long timePoint = 0;    // current sense and diagnosis,if you want to use this 
   if(millis() - timePoint > 1000){       //function,please show it & don't forget to connect the IS pins to Arduino                                             
     current_sense();
     timePoint = millis();
   }
    
  if(Serial.available()){
    char val = Serial.read();
    if(val != -1)
    {
      switch(val)
      {
      case 'w'://Move Forward
        advance (motorSpeed,motorSpeed);   //move forward in max speed
        break;
      case 's'://Move Backward
        back_off (motorSpeed,motorSpeed);   //move back in max speed
        break;
      case 'z':
        Serial.println("Hello");
        break;
      case 'x':
        stop();
        break;
      case 'q':
         motorSpeed += 5;
         analogWrite (E1,motorSpeed);
         Serial.println(motorSpeed);
         break;
       case 'e':
         motorSpeed -= 5;
         analogWrite (E1,motorSpeed);
         Serial.println(motorSpeed);
         break;
      }
    }
    else stop();  
  }
}
