boolean isHumanDetected = true;
int val = 0;
int pirPin = 12;
int potPin = 0;                           // Analog pin 0 connected to the potentiometer
int transistorPin = 9;                  // connected from digital pin 9 to the base of the transistor
int potValue = 0;  
// value returned from the potentiometer

void setup() 
{
  Serial.begin(9600);
  pinMode(transistorPin, OUTPUT);
  pinMode(pirPin, INPUT); 
}

void loop()
{
    int sensorVal = digitalRead(pirPin);
    HumanDetected(sensorVal == HIGH);
}

void HumanDetected(boolean isDetected)
{
  if(isDetected != isHumanDetected)
  {
    isHumanDetected = isDetected;
    if(isHumanDetected)
    {
      analogWrite(transistorPin, 255); 
      Serial.println("Human detected");
    }
    else
    {
      analogWrite(transistorPin, 0);
      Serial.println("Waiting for next human...");
    }
  } 
}
